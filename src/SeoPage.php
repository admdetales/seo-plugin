<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin;

class SeoPage
{
    /**
     * @var array
     */
    private $headAttributes = [];

    /**
     * @var array
     */
    private $htmlAttributes = [];

    /**
     * @var array
     */
    private $langAlternates = [];

    /**
     * @var string
     */
    private $linkCanonical;

    /**
     * @var array
     */
    private $links = [];

    /**
     * @var array
     */
    private $metas;

    /**
     * @var string
     */
    private $separator;

    /**
     * @var string
     */
    private $title;

    /**
     * @param string $title
     * @param string $separator
     */
    public function __construct(string $title = '', string $separator = ' | ')
    {
        $this->title = $title;
        $this->separator = $separator;
        $this->metas = [
            'http-equiv' => [],
            'name' => [],
            'schema' => [],
            'charset' => [],
            'property' => [],
        ];
    }

    /**
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function addHeadAttribute(string $name, string $value): self
    {
        $this->headAttributes[$name] = $value;

        return $this;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setHeadAttributes(array $attributes): self
    {
        $this->headAttributes = $attributes;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeadAttributes(): array
    {
        return $this->headAttributes;
    }

    /**
     * @param string $name
     * @param string|null $value
     * @return $this
     */
    public function addHtmlAttributes(string $name, ?string $value): self
    {
        $this->htmlAttributes[$name] = $value;

        return $this;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setHtmlAttributes(array $attributes): self
    {
        $this->htmlAttributes = $attributes;

        return $this;
    }

    /**
     * @return array
     */
    public function getHtmlAttributes(): array
    {
        return $this->htmlAttributes;
    }

    /**
     * @param string $href
     * @param string $hrefLang
     * @return $this
     */
    public function addLangAlternate(string $href, string $hrefLang): self
    {
        $this->langAlternates[$href] = $hrefLang;

        return $this;
    }

    /**
     * @param array $langAlternates
     * @return $this
     */
    public function setLangAlternates(array $langAlternates): self
    {
        $this->langAlternates = $langAlternates;

        return $this;
    }

    /**
     * @return array
     */
    public function getLangAlternates(): array
    {
        return $this->langAlternates;
    }

    /**
     * @param string $linkCanonical
     * @return $this
     */
    public function setLinkCanonical(string $linkCanonical): self
    {
        $this->linkCanonical = $linkCanonical;

        return $this;
    }

    /**
     * @return string
     */
    public function getLinkCanonical(): string
    {
        return $this->linkCanonical;
    }

    /**
     * @param array $links
     * @return $this
     */
    public function setLinks(array $links): self
    {
        $this->links = $links;

        return $this;
    }

    /**
     * @return array
     */
    public function getLinks(): array
    {
        return $this->links;
    }

    /**
     * @param string $rel
     * @param string $href
     * @return $this
     */
    public function addLink(string $rel, string $href): self
    {
        $this->links[$rel] = $href;

        return $this;
    }

    /**
     * @return array
     */
    public function getMetas(): array
    {
        return $this->metas;
    }

    /**
     * @param string $type
     * @param string $name
     * @param string $content
     * @param array $extras
     * @return $this
     */
    public function addMeta(string $type, string $name, string $content, array $extras = []): self
    {
        if (false === isset($this->metas[$type])) {
            $this->metas[$type] = [];
        }

        $this->metas[$type][$name] = [$content, $extras];

        return $this;
    }

    /**
     * @param array $metas
     * @return $this
     */
    public function setMetas(array $metas): self
    {
        $this->metas = [];

        foreach ($metas as $type => $metadata) {
            if (!is_array($metadata)) {
                throw new \RuntimeException(sprintf('%s must be an array', $metadata));
            }

            foreach ($metadata as $name => $meta) {
                list($content, $extras) = $this->normalizeMeta($meta);

                $this->addMeta($type, $name, $content, $extras);
            }
        }

        return $this;
    }

    /**
     * @param string $separator
     * @return $this
     */
    public function setSeparator(string $separator): self
    {
        $this->separator = $separator;

        return $this;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function addTitle(string $title): self
    {
        $this->title = $title . $this->separator . $this->title;

        return $this;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return rtrim($this->title, $this->separator);
    }

    /**
     * @param mixed $meta
     * @return array
     */
    private function normalizeMeta($meta): array
    {
        if (is_string($meta)) {
            return array($meta, []);
        }

        return $meta;
    }
}
