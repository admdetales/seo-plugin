<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin\Twig;

use Omni\Sylius\SeoPlugin\Templating\SeoEngine;

class SeoExtension extends \Twig_Extension
{
    /**
     * @var SeoEngine
     */
    private $seoEngine;

    /**
     * @param SeoEngineInterface $seoEngine
     */
    public function __construct(SeoEngine $seoEngine)
    {
        $this->seoEngine = $seoEngine;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'omni_seo_head_attributes',
                [$this->seoEngine, 'renderHeadAttributes'],
                ['is_safe' => ['html']]
            ),
            new \Twig_SimpleFunction(
                'omni_seo_html_attributes',
                [$this->seoEngine, 'renderHtmlAttributes'],
                ['is_safe' => ['html']]
            ),
            new \Twig_SimpleFunction(
                'omni_seo_lang_alternates',
                [$this->seoEngine, 'renderLangAlternates'],
                ['is_safe' => ['html']]
            ),
            new \Twig_SimpleFunction(
                'omni_seo_link_canonical',
                [$this->seoEngine, 'renderLinkCanonical'],
                ['is_safe' => ['html']]
            ),
            new \Twig_SimpleFunction(
                'omni_seo_links',
                [$this->seoEngine, 'renderLinks'],
                ['is_safe' => ['html']]
            ),
            new \Twig_SimpleFunction(
                'omni_seo_meta',
                [$this->seoEngine, 'renderMeta'],
                ['is_safe' => ['html']]
            ),
            new \Twig_SimpleFunction(
                'omni_seo_title',
                [$this, 'renderTitle'],
                ['is_safe' => ['html']]
            ),
        ];
    }

    /**
     * @param string|null $default
     * @return string
     */
    public function renderTitle(?string $default): string
    {
        $title = $this->seoEngine->renderTitle();

        if ('<' === $title[7]) {
            $title = sprintf("<title>%s</title>\n", $default);
        }

        return $title;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'omni_seo';
    }
}
