<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin\Form\DataTransformer;

use Omni\Sylius\SeoPlugin\Model\KeyValueArray;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class KeyValueTransformer implements DataTransformerInterface
{
    /**
     * @var bool
     */
    private $useKeyValueArray;

    /**
     * @param bool $useKeyValueArray
     */
    public function __construct(bool $useKeyValueArray)
    {
        $this->useKeyValueArray = $useKeyValueArray;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($keyValues)
    {
        return $keyValues;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($keyValues)
    {
        $data = $this->useKeyValueArray ? new KeyValueArray() : [];

        foreach ($keyValues as $keyValueRow) {
            if ((false === is_array($keyValueRow)) || ['key', 'value'] != array_keys($keyValueRow)) {
                throw new TransformationFailedException('Key and value is not valid!');
            }

            if (array_key_exists($keyValueRow['key'], $data)) {
                throw new TransformationFailedException(sprintf('Duplicate %s key detected!', $keyValueRow['key']));
            }

            $data[$keyValueRow['key']] = $keyValueRow['value'];
        }

        return $data;
    }
}
