<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin;

use Omni\Sylius\SeoPlugin\Model\SeoMetadataInterface;

class SeoPresentation
{
    /**
     * @var string
     */
    private $encoding;

    /**
     * @var SeoPage
     */
    private $seoPage;

    /**
     * @param string $encoding
     * @param SeoPage $seoPage
     */
    public function __construct(string $encoding, SeoPage $seoPage)
    {
        $this->encoding = $encoding;
        $this->seoPage = $seoPage;
    }

    /**
     * @param SeoMetadataInterface $seoMetadata
     */
    public function updateSeoPage(SeoMetadataInterface $seoMetadata): void
    {
        $this->seoPage->addHtmlAttributes('lang', $seoMetadata->getLang());
        $this->seoPage->addMeta('charset', $this->encoding, '');

        if ($description = $seoMetadata->getMetaDescription()) {
            $this->seoPage->addMeta('name', 'description', $description);
        }

        if ($extraHttp = $seoMetadata->getExtraHttp()) {
            $this->addMetas('http-equiv', $extraHttp);
        }

        if ($extraNames = $seoMetadata->getExtraNames()) {
            $this->addMetas('name', $extraNames);
        }

        if ($extraProperties = $seoMetadata->getExtraProperties()) {
            $this->addMetas('property', $extraProperties);
        }

        if ($keywords = $seoMetadata->getMetaKeywords()) {
            $this->seoPage->addMeta('name', 'keywords', $keywords);
        }

        if ($robots = $seoMetadata->getMetaRobots()) {
            $this->seoPage->addMeta('name', 'robots', $robots);
        }

        if ($title = $seoMetadata->getTitle()) {
            $this->addTitle($title);
        }
    }

    /**
     * @param string $type
     * @param array $values
     */
    private function addMetas(string $type, array $values): void
    {
        foreach ($values as $key => $value) {
            $this->seoPage->addMeta($type, $key, $value);
        }
    }

    /**
     * @param string $title
     */
    private function addTitle(string $title): void
    {
        $this->seoPage->addTitle($title);
        $this->seoPage->addMeta('name', 'title', $title);
    }
}
